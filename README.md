# GeoMonitor da Saúde

Uma aplicação para visualização espacial de dados de internações hospitalares da cidade de São Paulo.

## Cobertura de testes

Development:
[![pipeline status](https://gitlab.com/interscity/health-dashboard/health-smart-city/badges/development/pipeline.svg)](https://gitlab.com/interscity/health-dashboard/health-smart-city/commits/master)
[![coverage report](https://gitlab.com/interscity/health-dashboard/health-smart-city/badges/development/coverage.svg)](https://gitlab.com/interscity/health-dashboard/health-smart-city/commits/master)

Master:
[![pipeline status](https://gitlab.com/interscity/health-dashboard/health-smart-city/badges/master/pipeline.svg)](https://gitlab.com/interscity/health-dashboard/health-smart-city/commits/master)
[![coverage report](https://gitlab.com/interscity/health-dashboard/health-smart-city/badges/master/coverage.svg)](https://gitlab.com/interscity/health-dashboard/health-smart-city/commits/master)

## O GeoMonitor

É possível acessar o GeoMonitor da Saúde através dos seguintes links:

**Produção:** [http://interscity.org/apps/saude](http://interscity.org/apps/saude)

**Homologação:** [http://teste.healthdashboard.interscity.org](http://teste.healthdashboard.interscity.org)

### Contribuindo

Informações sobre como contribuir com o projeto podem ser encontradas [aqui](./CONTRIBUTING.md).
